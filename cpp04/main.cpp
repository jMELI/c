#include <iostream>
#include <string>
#include <sstream>

using namespace std;

#include "Account.h"


void test();


int main()
{
    test();
    return 0;
}

//Following code is not important
//Global variable, never ever do this!
bool ok = true;

string toString(int i)
{
    stringstream ss;
    ss << i;
    return ss.str();
}

void addError(string& result, string message, string value){
    result.append("TEST FAILED: ");
    result.append(message);
    result.append(" Value returned was: ");
    result.append(value);
    result.append("\n");
    ok = false;
}

bool runTests(){

    string result = "";

    Account acc("Jan Adler", 5000, 1234);
    int value = acc.getMoney();
    string svalue = acc.getOwner();

    if(value != 5000) addError(result, "Method getMoney should return 5000", toString(value));
    if(svalue != "Jan Adler") addError(result, "Method getOwner should return \"Jan Adler\"", svalue);
    acc.deposit(1000);
    value = acc.getMoney();
    if(value != 6000) addError(result, "Method getMoney should return 6000", toString(value));
    acc.deposit(5);
    value = acc.getMoney();
    if(value != 6005) addError(result, "Method getMoney should return 6005", toString(value));
    bool succ = acc.withdraw(1000,1234);
    if(!succ) addError(result, "Method withdraw should return true", succ ? "true" : "false");
    value = acc.getMoney();
    if(value != 5005) addError(result, "Method getMoney should return 5005", toString(value));
    succ = acc.withdraw(10000, 1234);
    if(succ) addError(result, "Method withdraw should return false", succ ? "true" : "false");
    value = acc.getMoney();
    if(value != 5005) addError(result, "Method getMoney should return 5005", toString(value));
    succ = acc.withdraw(1000, 1111);
    if(succ) addError(result, "Method withdraw should return false", succ ? "true" : "false");
    value = acc.getMoney();
    if(value != 5005) addError(result, "Method getMoney should return 5005", toString(value));
    cout<<result<<endl;
    return ok;
}

void test(){

    if(runTests()){
        cout<<"Testy skoncily uspesne!" << endl;
    }
    else{
        cout<<"Testy selhaly." << endl;
    }
}
