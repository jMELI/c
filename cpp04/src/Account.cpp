#include <iostream>
#include "Account.h"

Account::Account(string owner, int money, int pin)
{
    this->owner = owner;
    this->money = money;
    this->pin = pin;
}

Account::~Account()
{
    //destructor
}

int Account::getMoney(){
    return money;
}

string Account::getOwner(){
    return owner;
}

void Account::deposit(int amount){
    money += amount;
}

bool Account::withdraw(int amount, int pin){

    if(pin != this->pin) return false;
    if(money - amount < 0) return false;

    money -= amount;
    return true;
}

void Account::print(){

}
