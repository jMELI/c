#ifndef ACCOUNT_H
#define ACCOUNT_H

using namespace std;

class Account
{
    string owner;
    int money;
    int pin;

    public:
        Account(string ,int, int);
        int getMoney();
        string getOwner();
        bool withdraw(int, int);
        void deposit(int);
        void print();
        ~Account();
};

#endif // ACCOUNT_H
