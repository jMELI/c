#ifndef CUBEGENERATOR_H
#define CUBEGENERATOR_H

#include "Assets.h"
#include "Texture.h"
#include "Cube.h"

class CubeGenerator
{
    public:

    static Cube createGrass(){
        Cube grass = Cube(Assets::grassTop.getTexture(), Assets::grassSide.getTexture());
        return grass;
    }

    static Cube createStone(){
        Cube stone = Cube(Assets::stone.getTexture(), Assets::stone.getTexture());
        return stone;
    }

    static Cube createWood(){
        Cube wood = Cube(Assets::woodUp.getTexture(), Assets::woodSide.getTexture());
        return wood;
    }

    static Cube createLeaves(){
        Cube leaves = Cube(Assets::leaves.getTexture(), Assets::leaves.getTexture());
        return leaves;
    }

    static Cube createCobblestone(){
        Cube cobble = Cube(Assets::cobblestone.getTexture(), Assets::cobblestone.getTexture());
        return cobble;
    }

    static Cube createDiamond(){
        Cube dia = Cube(Assets::diamond.getTexture(), Assets::diamond.getTexture());
        return dia;
    }

    static Cube createIron(){
        Cube iron = Cube(Assets::iron.getTexture(), Assets::iron.getTexture());
        return iron;
    }

    static Cube createSand(){
        Cube sand = Cube(Assets::sand.getTexture(), Assets::sand.getTexture());
        return sand;
    }

};

#endif // CUBEGENERATOR_H
