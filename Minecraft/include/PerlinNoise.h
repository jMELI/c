#ifndef PERLINNOISE_H
#define PERLINNOISE_H

#include <vector>

class PerlinNoise
{

    public:

    static const int DIMENSION_X = World::WORLD_SIZE_X;
    static const int DIMENSION_Y = World::WORLD_SIZE_Z;

    double terrain[DIMENSION_X][DIMENSION_Y];

    void createPerlin(){
        for(int i = 0; i < DIMENSION_X; i++){
            for(int j = 0; j < DIMENSION_Y; j++){
                terrain[i][j] = 0;
            }
        }

        int octaves = 5;
        float persisitence = 0.55;

        for(int k = 0 ; k < octaves; k++){
            float frequency = pow(2,k);
            float amplitude = pow(persisitence,k);

            std::vector<std::vector<float>> noise;
            for(int i = 0; i <= frequency; i++){
                noise.emplace_back(std::vector<float>());
                for(int j = 0; j <= frequency; j++){
                    noise[i].emplace_back(((rand()%1000)/1000.0)*amplitude);
                }
            }

            float nx = static_cast<float>(DIMENSION_X) / frequency;
            float ny = static_cast<float>(DIMENSION_Y) / frequency;

            for (int ky = 0; ky < DIMENSION_Y; ky++){
                for (int kx = 0; kx < DIMENSION_X; kx++){
                    int i = int(kx / nx);
                    int j = int(ky / ny);
                    //bilinear interpolation
                    float dx0 = kx - i * nx;
                    float dx1 = nx - dx0;
                    float dy0 = ky - j * ny;
                    float dy1 = ny - dy0;
                    float z = noise[j][i] * dx1 * dy1;
                    z += noise[j][i + 1] * dx0 * dy1;
                    z += noise[j + 1][i] * dx1 * dy0;
                    z += noise[j + 1][i + 1] * dx0 * dy0;
                    z /= nx * ny;
                    terrain[ky][kx] += z; //add layers together
                }
            }

        }
    }

    void normalize(){
        double maximum = 0;
        for(int i = 0; i < DIMENSION_X; i++){
            for(int j = 0; j < DIMENSION_Y; j++){
                if(terrain[i][j] > maximum) maximum = terrain[i][j];
            }
        }

        double mul = 1.0 / maximum;
        for(int i = 0; i < DIMENSION_X; i++){
            for(int j = 0; j < DIMENSION_Y; j++){
                terrain[i][j]*=mul;
            }
        }
    }


};

#endif // PERLINNOISE_H
