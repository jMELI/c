#ifndef IRONBLOCK_H
#define IRONBLOCK_H


class IronBlock : public Block
{
    public:
        IronBlock() : Block(CubeGenerator::createIron(), BlockType::IronBlock) {}

        bool collides() const override{
            return true;
        }
};

#endif // IRONBLOCK_H
