#ifndef DIAMONDBLOCK_H
#define DIAMONDBLOCK_H


class DiamondBlock : public Block
{
    public:
        DiamondBlock() : Block(CubeGenerator::createDiamond(), BlockType::DiamondBlock) {}

        bool collides() const override {
            return true;
        }
};

#endif // DIAMONDBLOCK_H
