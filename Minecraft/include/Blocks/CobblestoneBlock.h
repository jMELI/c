#ifndef COBBLESTONEBLOCK_H
#define COBBLESTONEBLOCK_H


class CobblestoneBlock : public Block
{
    public:
        CobblestoneBlock() : Block(CubeGenerator::createCobblestone(), BlockType::CobblestoneBlock){
        }

        bool collides() const override{
            return true;
        }

};

#endif // COBBLESTONEBLOCK_H
