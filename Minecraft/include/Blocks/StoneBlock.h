#ifndef STONEBLOCK_H
#define STONEBLOCK_H

#include "Block.h"

class StoneBlock : public Block
{
    public:
        StoneBlock() : Block(CubeGenerator::createStone(), BlockType::StoneBlock){

        }

        bool collides() const override{
            return true;
        }

};

#endif // STONEBLOCK_H
