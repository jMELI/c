#include "include/World.h"
#include "include/PerlinNoise.h"
#include <time.h>

void World::create(){
    srand(time(nullptr));
    player.setPosition(30,60,30);
    PerlinNoise noise;
    noise.createPerlin();
    noise.normalize();

    for(unsigned x = 0; x < WORLD_SIZE_X; x++){
        for(unsigned z = 0; z < WORLD_SIZE_Z; z++){
            int height = noise.terrain[x][z] * WORLD_SIZE_Y/2.0;
            if(height < 0) continue;
            for(int y = 0; y < height; y++){
                setBlockAt(x, y, z, BlockType::StoneBlock);
            }
            if(height - 1 > WORLD_SIZE_Y/3.0){
                setBlockAt(x,height - 1, z, BlockType::GrassBlock);
                if(rand()%200 == 0) addTree(x, height, z);
            }
            else{
                setBlockAt(x,height - 1, z, BlockType::SandBlock);
            }
        }
    }

    for(int i = 0; i < 100; i++){
        int x = rand()%WORLD_SIZE_X;
        int z = rand()%WORLD_SIZE_Z;
        int y = 0;
        int amount = 10;
        BlockType type;
        switch(rand()%3){
            case 0: type = BlockType::DiamondBlock;
                    amount = rand()%5 + 3;
                    y = rand()%(int)(noise.terrain[x][z] * WORLD_SIZE_Y/3.0);
            break;
            case 1: type = BlockType::IronBlock;
                    amount = rand()%8 + 4;
                    y = rand()%(int)(noise.terrain[x][z] * WORLD_SIZE_Y/2.0);
            break;
            case 2: type = BlockType::AirBlock;
                    amount = 200 + rand()%500;
                    y = rand()%(int)(noise.terrain[x][z] * WORLD_SIZE_Y/2.0);
                    break;
        }

        int ox = x;
        int oz = z;
        int oy = y;
        for(int a = 0; a < amount; a++){
            switch(rand()%3){
                case 0: x += rand()%3 - 1; break;
                case 1: y += rand()%3 - 1; break;
                case 2: z += rand()%3 - 1; break;
            }
            if(abs(x - ox) + abs(y - oy) + abs(z-oz) > 10){
                x = ox;
                y = oy;
                z = oz;
            }
            if(type == BlockType::AirBlock){
                setBlockAt(x+1,y,z,type);
                setBlockAt(x+1,y+1,z,type);
                setBlockAt(x+1,y+1,z+1,type);
                setBlockAt(x,y+1,z,type);
                setBlockAt(x,y+1,z+1,type);
                setBlockAt(x,y,z+1,type);
                setBlockAt(x+1,y,z+1,type);
            }
            setBlockAt(x,y,z,type);
        }
    }

    calculateVisibility();
}

void World::addTree(int x, int y, int z){
    int height = rand()%5 + 4;

    for(int i = 0; i < height; i++){
        setBlockAt(x, y + i, z, BlockType::WoodBlock);
    }

    for(int i = height - 2; i < height + 2; i++){
        setBlockAt(x - 1, y + i, z, BlockType::LeafBlock);
        setBlockAt(x + 1, y + i, z, BlockType::LeafBlock);
        setBlockAt(x, y + i, z - 1, BlockType::LeafBlock);
        setBlockAt(x, y + i, z + 1, BlockType::LeafBlock);
        if(rand()%(abs((i+1) - height) + 1) == 0){
            setBlockAt(x - 2, y + i, z, BlockType::LeafBlock);
            setBlockAt(x + 2, y + i, z, BlockType::LeafBlock);
            setBlockAt(x, y + i, z - 2, BlockType::LeafBlock);
            setBlockAt(x, y + i, z + 2, BlockType::LeafBlock);
            if(rand()%(abs(i - height) + 1) == 0){
                setBlockAt(x - 1, y + i, z - 1, BlockType::LeafBlock);
                setBlockAt(x + 1, y + i, z - 1, BlockType::LeafBlock);
                setBlockAt(x - 1, y + i, z + 1, BlockType::LeafBlock);
                setBlockAt(x + 1, y + i, z + 1, BlockType::LeafBlock);
            }
        }
    }

    for(int i = height; i < height + 2; i++){
        setBlockAt(x, y + i, z, BlockType::LeafBlock);
    }
}
