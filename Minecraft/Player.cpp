#include "include/Player.h"
#include "include/World.h"

void Player::update(float delta){
    if(delta > 1) return;
    move(delta);
    velocityY += gravity*delta;
    if(world.isBlockAt(position.x, position.y - 1, position.z) && velocityY < 0){
        velocityY = 0;
    }
    position.y += velocityY * delta;
}

void Player::move(float delta){
    if(moving != 0){
        float fx = position.x + moving * delta * speed * cos(Utils::degreesToRad(rotation));
        float fz = position.z + moving * delta * speed * sin(Utils::degreesToRad(rotation));
        float cx = position.x + moving * 5*delta * speed * cos(Utils::degreesToRad(rotation));
        float cz = position.z + moving * 5*delta * speed * sin(Utils::degreesToRad(rotation));
        if(!world.isBlockAt(cx, position.y, cz)){
                position.x = fx;
                position.z = fz;
        }
    }
}
