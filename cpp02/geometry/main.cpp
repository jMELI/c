#include <iostream>

using namespace std;

/*Implement following functions*/

/*Draws line of given length. Eg: drawLine(3) ->

 ***

 */
void drawLine(int len){
    for(int i =0; i<len; i++) cout<<"#";
    cout<<endl;
}

void drawEmptySpace(int len){
    for(int i =0; i<len; i++) cout<<" ";
}

/*Draws square of given side. Eg: drawSquare(3) ->

***
***
***

*/

void drawSquare(int a){
    for(int i = 0; i < a; i++){
        drawLine(a);
    }
}

/*Draws rectangle of given dimension. Eg: drawRectangle(5, 3) ->

*****
*****
*****

*/
void drawRectangle(int width, int height)
{
    for(int i = 0; i < height; i++){
        drawLine(width);
    }
}

/*Draws triangle of given height. Eg: drawTriangle(4) ->

*
**
***
****

*/
void drawTriangle(int height){
    for(int i = 0; i < height; i++){
        drawLine(i+1);
    }
}

/*Draws pyramid of given height. Eg: drawPyramid(4) ->

   *
  ***
 *****
*******

*/

void drawPyramid(int height){
    for(int i = 0; i < height; i++){
        drawEmptySpace(height-1-i);
        drawLine(1 + 2*i);
    }
}

/*Draws empty pyramid of given height. Eg: drawEmptyPyramid(4) ->

   *
  * *
 *   *
*******

*/
void drawEmptyPyramid(int height){
    for(int i = 0; i < height - 1; i++){
        drawEmptySpace(height-1-i);
        cout<<"#";
        drawEmptySpace(2*i - 1);
        if(i > 0){
            cout<<"#";
        }
        cout<<endl;
    }
    drawLine(2*height - 1);
}

int main()
{
    drawLine(7);
    cout<<endl<<endl;
    drawSquare(3);
    cout<<endl<<endl;
    drawRectangle(12,3);
    cout<<endl<<endl;
    drawTriangle(5);
    cout<<endl<<endl;
    drawPyramid(5);
    cout<<endl<<endl;
    drawEmptyPyramid(7);
    return 0;
}
