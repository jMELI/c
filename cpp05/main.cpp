#include <iostream>
#include <string>
#include <sstream>

using namespace std;

#include "Account.h"
#include "SavingsAccount.h"
#include "OverdraftAccount.h"


int main()
{
    Account account("Jan Adler", 5000, 1889);
    //SavingsAccount savings("Peta Korunka", 1000, 3762, 2.5);      //You can use this code after SavingsAccount implementation
    //OverdraftAccount overdraft("Joza!!!!", -500, 1234, -3000, 5); //You can use this code after OverdraftAccount implementation

    return 0;
}
