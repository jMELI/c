#ifndef OVERDRAFTACCOUNT_H
#define OVERDRAFTACCOUNT_H

#include <Account.h>


class OverdraftAccount : public Account
{
    int limit;
    double overdraftInterest;
    public:
        OverdraftAccount(string, int, int, int, double);
        bool withdraw(int, int);
        void applyInterest();
};

#endif // OVERDRAFTACCOUNT_H
