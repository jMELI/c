#ifndef SAVINGSACCOUNT_H
#define SAVINGSACCOUNT_H

#include "Account.h"


class SavingsAccount : public Account
{
    double interest;
    public:
        SavingsAccount(string, int, int, double);
        applyInterest();
    private:
};

#endif // SAVINGSACCOUNT_H
